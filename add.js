// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
//
let firstNum  = parseInt(prompt('Enter first number: '));
while (!(firstNum) &&  isNaN(firstNum)) {
     firstNum = parseInt(prompt('ERROR enter number: '));
}
let secondNum  = parseInt(prompt('Enter second number: '));
while (!(firstNum) &&  isNaN(firstNum)) {
     firstNum = parseInt(prompt('ERROR enter number:: '));
}
function math() {
     let sum = firstNum/secondNum
return alert(`Частка від ділення ${firstNum} на ${secondNum} = ${sum}`)
}
math();

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
//     Технічні вимоги:
//     - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.
// let firstNum = parseInt(prompt('Enter first number: '));
//
while (isNaN(firstNum) || firstNum === null || firstNum === '') {
    firstNum = parseInt(prompt('ERROR: Please enter a valid number: '));
}

let secondNum = parseInt(prompt('Enter second number: '));

while (isNaN(secondNum) || secondNum === null || secondNum === '') {
    secondNum = parseInt(prompt('ERROR: Please enter a valid number: '));
}

let mathOperation = prompt('Please enter math operation, like this +, -, / ,* :');

while (!(mathOperation === '+' || mathOperation === '-' || mathOperation === '/' || mathOperation === '*')) {
    if (mathOperation === null) {
        alert('Goodbye');
        break;
    }
    mathOperation = prompt('ERROR: Please enter a valid math operation, like this +, -, / ,* :');
}
function count() {
    switch (mathOperation) {
        case '+':
            let sum = firstNum + secondNum;
            alert(`${firstNum} + ${secondNum} = ${sum}`);
            break;
        case '-':
            let min = firstNum - secondNum;
            alert(`${firstNum} - ${secondNum} = ${min}`);
            break;
        case "/":
            if (secondNum !== 0 && firstNum !== 0) {
                let dev = firstNum / secondNum;
                alert(`${firstNum} / ${secondNum} = ${dev}`);
            } else {
                alert("Не ділиться на нуль");
            }
            break
        case '*':
            let multipli = firstNum*secondNum;
            alert(`${firstNum} * ${secondNum} = ${multipli}`);
            break;
    }
}
count()
// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
//     Технічні вимоги:
//     - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

function factorial() {
    let number = +prompt('Enter first number: ');
    let count = 1;
    for (let i = 1; i <= number; i++) {
     count*=i;
    }
    alert(`Факториал введеного числа ${number} = ${count}`);

}
factorial()
